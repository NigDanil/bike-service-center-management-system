<h4>Clone Project</h4>

- [ ] Clone with SSH

```bash
git@gitlab.com:NigDanil/bike-service-center-management-system.git
```

- [ ] Clone with HTTPS

```bash
https://gitlab.com/NigDanil/bike-service-center-management-system.git
```

<center><h1>Cистемы управления центром обслуживания велосипедов<br>
(СУ ЦОВ)</h1></center>

<center>

[Содержание](data/content.md)

</center>
