[Содержание](content.md) &brvbar; [вперед](1_2.md)

<hr>

<h1>Диаграмма активности СУ ЦОВ
</h1>

<p>Это UML-диаграмма активности <b>СУ ЦОВ</b>, которая показывает потоки между действиями по <b>Repair Bike, Delivery, Insurance, Payment</b>. Основные действия, связанные с этой диаграммой действий UML <b>СУ ЦОВ</b>, следующие:
</p>

- Repair Bike Activity
- Delivery Activity
- Insurance Activity
- Payment Activity
- Bike Activity

<h4>Особенности UML-диаграммы активности СУ ЦОВ
</h4>

- Пользователь с правами администратора может искать <b>Bike</b> подлежащий ремонту, просматривать описание выбранного <b>Bike</b>, добавлять, обновлять, а так же удалять <b>Bike</b>.
- Демонстрирует поток действий по редактированию, добавлению и обновлению <b>Insurance</b>.
- Пользователь сможет искать и создавать отчеты о <b>Insurance и Payment</b> <b>Bike</b>.
- Все объекты, такие как <b>(Repair Bike, Delivery, Bike)</b> взаимосвязаны.
- Демонстрирует полное описание и порядок операций <b>«Repair Bike»</b>, <b>Payment</b>, <b>«Bike»</b>, <b>Insurance</b>, <b>Delivery</b>.

<center>

![title 1](../img/1_1.drawio.svg)

</center>

<center><b>Диаграмма активности СУ ЦОВ</b></center>

<hr>

[Содержание](content.md) &brvbar; [вперед](1_2.md)
